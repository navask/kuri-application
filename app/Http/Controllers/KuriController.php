<?php

namespace App\Http\Controllers;

use App\Models\Kuri;
use App\Models\User;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\QueryException;

class KuriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'userName' => 'required|email|unique:kuris,userName',
            'password' => 'required|min:8',
            'confirm' => 'required|same:password',
            'name' => 'required',
            'contact' => 'required|numeric',
            'one_lot_amt' => 'required',
            'total_kuri_amt' => 'required',
            'start_date' => 'required',
            'address' => 'required',
            'no_of_lots' => 'required|numeric',
            'type' => 'required',
            'roganisor_lot' => 'required',
            'acceptTerms' => 'required|accepted',
        ];

        // Custom validation messages
        $messages = [
            'userName.unique' => 'Username already exists.',
            'confirm.same' => 'The confirmation password does not match.',
        ];

        // Validate the request data
        $validatedData = $request->validate($rules, $messages);

        $create_user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['userName'],
            'password' => bcrypt($validatedData['password'])

        ]);

        unset($validatedData['password']);

        $chitti = Kuri::create($validatedData);

        $tableName = str_replace(' ', '_', trim($request->input('name')));
        $numberOfColumns = $request->input('no_of_lots', 0);

        // Append a timestamp to make the table name unique
        $timestamp = now()->timestamp;
        $uniqueTableName = $tableName . '_' . $timestamp;

        if (!Schema::hasTable($uniqueTableName)) {
            Schema::create($uniqueTableName, function (Blueprint $table) use ($numberOfColumns) {
                $table->id();
                $table->string('member_name');
                $table->string('contact');
                $table->string('kuri_amount');
                $table->integer('lot_number')->nullable();
                for ($i = 1; $i <= $numberOfColumns; $i++) {
                    $columnName = 'lot_' . $i;
                    $table->timestamp($columnName)->nullable();

                    $payModeColumnName = 'pay_mode_' . $i;
                    $table->text($payModeColumnName, 25)->nullable();

                    $noteColumnName = 'note_' . $i;
                    $table->text($noteColumnName, 100)->nullable();
                }
                $table->string('created_by');
                $table->unsignedBigInteger('kuri_id');

                $table->foreign('kuri_id')->references('id')->on('kuris');

                $table->timestamps();
            });
            $lastInsertedId = Kuri::latest('id')->first()->id;

            Kuri::where('id', $lastInsertedId)->update(['table_name' => $uniqueTableName]);

            return response()->json(['message' => 'Chitty created successfully.'], 201);
        } else {
            return response()->json(['message' => 'Chitty already exists. Please choose a different Chitty name.'], 400);
        }
    }
    public function addMember(Request $request)
    {

        // Validation rules
        $rules = [
            'name' => 'required',
            'contact' => 'required',
            'lot_number' => 'required',
            'kuri_amount' => 'required',
        ];

        // Custom validation messages
        $messages = [
            'name' => 'Member Name Required.',
            'lot_number' => 'Lot Number Required',
            'contact' => 'Member Contact Required',
            'kuri_amount' => 'Kuri Amount Required',
        ];

        $validatedData = $request->validate($rules, $messages);
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $kuri_id = Kuri::where('table_name', $table)->first(['id']);
        $kuri_id = $kuri_id->id;

        $mode = trim($request->post('mode'));
        $member_id = trim($request->post('member_id'));

        if ($table) {
            if ($mode == 'edit') {
                DB::table($table)->where(['id' => $member_id])->update([
                    'member_name' => $validatedData['name'],
                    'contact' => $validatedData['contact'],
                    'lot_number' => $validatedData['lot_number'],
                    'kuri_amount' => $validatedData['kuri_amount'],
                ]);

                $msg = 'Member details updated successfully.';
            } else {
                DB::table($table)->insert([
                    'member_name' => $validatedData['name'],
                    'contact' => $validatedData['contact'],
                    'lot_number' => $validatedData['lot_number'],
                    'kuri_amount' => $validatedData['kuri_amount'],
                    'created_by' => auth()->user()->email,
                    'kuri_id' => $kuri_id,
                ]);
                $msg = 'Member added successfully.';
            }


            return redirect()->route('dashboard')->with('success', $msg);
        } else {
            return redirect()->route('dashboard')->with('failed', 'Failed..!');
        }
    }

    public function mark_payment(Request $request)
    {
        $memId = $request->input('memId');
        $column = $request->input('lot_no');
        $colmn = trim(str_replace('lot_', '', $column));
        $selectedOption = $request->input('paymentOption');
        $note = $request->input('note');

        // var_dump($request->all());exit;

        $kuri = Kuri::where('username', auth()->user()->email)->first(['table_name']);

        if (!$kuri) {
            return redirect()->route('view-kuri')->with('failed', 'Kuri not found.');
        }

        $table = $kuri->table_name;

        // Use the Eloquent model to update the record
        $record = DB::table($table)->where('id', $memId);

        $save = $record->update([
            $column => now(),
            'pay_mode_' . $colmn => $selectedOption,
            'note_' . $colmn => $note,
        ]);

        if ($save) {
            return redirect()->route('view-kuri')->with('success', 'Payment Recorded Successfully.');
        } else {
            return redirect()->route('view-kuri')->with('failed', 'Payment Recorded failed.');
        }
    }

    public function mark_payment_individual(Request $request)
    {
        $memId = $request->input('member_select');
        $column = $request->input('lot_number_select');
        $colmn = trim(str_replace('lot_', '', $column));
        $selectedOption = $request->input('paymentmode_select');
        $note = $request->input('narration_textarea');

        $kuri = Kuri::where('username', auth()->user()->email)->first(['table_name']);

        if (!$kuri) {
            return redirect()->route('view-kuri')->with('failed', 'Kuri not found.');
        }

        $table = $kuri->table_name;

        // Use the Eloquent model to update the record
        $record = DB::table($table)->where('id', $memId);

        $save = $record->update([
            'lot_'.$colmn => now(),
            'pay_mode_' . $colmn => $selectedOption,
            'note_' . $colmn => $note,
        ]);

        if ($save) {
            return redirect()->route('mark-payment-individual')->with('success', 'Payment Recorded Successfully.');
        } else {
            return redirect()->route('mark-payment-individual')->with('failed', 'Payment Recorded failed.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function show(Kuri $kuri)
    {
        //
    }
    public function get_reports($lot_no)
    {
        $table = Kuri::where('username', auth()->user()->email)
            ->select('table_name', 'no_of_lots')
            ->first();

        $table_name = $table->table_name;
        $no_of_lots = $table->no_of_lots;
        $payment_types = DB::table('payment_modes')->where(['status' => 'active', 'created_by' => auth()->user()->email])->get();

        if ($lot_no == "0") {
            $view_type = "all";
            $data = DB::table($table_name)->get();
        } else {
            $data = DB::table($table_name)
                ->select('member_name', 'contact', 'lot_' . $lot_no, 'pay_mode_' . $lot_no, 'kuri_amount')
                ->get();
            $view_type = "individual";
        }

        return response()->json(['view_type' => $view_type, 'data' => $data, 'no_of_lots' => $no_of_lots, 'payment_types' => $payment_types]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $table = Kuri::where('username', auth()->user()->email)
            ->select('table_name', 'no_of_lots')
            ->first();

        $table_name = $table->table_name;
        $data = DB::table($table_name)
            ->select('id', 'member_name', 'contact', 'lot_number', 'kuri_amount')
            ->where(['id' => $id])
            ->first();
        return response()->json(['data' => $data]);
    }
    public function edit_pay_mode($id)
    {
        $data = DB::table('payment_modes')->where(['created_by' => auth()->user()->email, 'id' => $id])
            ->select('id', 'name')
            ->first();
        return response()->json(['data' => $data]);
    }
    public function get_date_and_amount($mem_id, $lot)
    {
        $table = Kuri::where('username', auth()->user()->email)
            ->select('table_name', 'no_of_lots', 'start_date')
            ->first();

        $table_name = $table->table_name;
        $data = DB::table($table_name)
            ->select('kuri_amount')
            ->where(['id' => $mem_id])
            ->first();

        $no_of_lots = $table->no_of_lots;
        $start_date = DateTime::createFromFormat('d-m-Y', $table->start_date);
        $sundays = [];

        for ($i = 0; $i < $no_of_lots; $i++) {
            $currentDate = clone $start_date;
            $currentDate->add(new DateInterval('P' . $i . 'W'));

            while ($currentDate->format('w') != 0) {
                $currentDate->add(new DateInterval('P1D'));
            }

            $sundays[] = $currentDate->format('d-m-Y');
        }
        return response()->json(['data' => $data,'date'=>$sundays[$lot-1]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kuri $kuri)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;

        $remove = DB::table($table)->where('id', $id)->delete();

        if ($remove) {
            return redirect()->route('dashboard')->with('success', 'Removed Member Successfully.');
        } else {
            return redirect()->route('dashboard')->with('failed', 'Failed..!');
        }
    }
    public function createPayMode(Request $request)
    {
        try {
            // Validation rules
            $rules = [
                'name' => 'required',
            ];

            // Custom validation messages
            $messages = [
                'name.required' => 'Payment Type Name is required.',
            ];

            // Validate the request data
            $validatedData = $request->validate($rules, $messages);

            // Get the Kuri table name and ID
            $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
            $table = $table->table_name;
            $kuri = Kuri::where('table_name', $table)->first(['id']);
            $mode = trim($request->post('mode'));
            $kuri_id = trim($request->post('kuri_id'));
            $kuri_name = trim($request->post('name'));
            if ($kuri) {
                if ($mode == 'edit') {
                    DB::table($table)->where(['id' => $kuri_id])->update([
                        'name' => $kuri_name,
                    ]);

                    $msg = 'Payment type updated successfully.';
                } else {
                    DB::table('payment_modes')->insert([
                        'name' => $validatedData['name'],
                        'created_by' => auth()->user()->email,
                        'kuri_id' => $kuri->id,
                    ]);
                    $msg = 'Payment type added successfully.';
                }
                return redirect()->route('payment_modes')->with('success', $msg);
            } else {
                return redirect()->route('payment_modes')->with('failed', 'Error while adding Payment Type.');
            }
        } catch (QueryException $e) {
            // Log or handle the exception as needed
            return redirect()->route('payment_modes')->with('failed', 'Error: Please Avoid Duplicate entry for Payment Types');
        }
    }
}
