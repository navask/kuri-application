<!DOCTYPE html>
<html>

@include('superadminlayout.head')

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')

            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Make New Chitty</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a>Generate Chitty</a>
                            </li>
                            <li class="active">
                                <strong>New Chitty</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Make New Chitty</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <h2>
                                        New Chitty Form
                                    </h2>
                                    <p>
                                        Complete all forms to make a new Chitty.
                                    </p>

                                    <form id="form" method="post" class="wizard-big">
                                        @csrf
                                        <h1>Create Account</h1>
                                        <fieldset>
                                            <h2>Account Information</h2>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label>Username *</label>
                                                        <input id="userName" name="userName" type="email" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password *</label>
                                                        <input id="password" name="password" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Confirm Password *</label>
                                                        <input id="confirm" name="confirm" type="text" class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="text-center">
                                                        <div style="margin-top: 20px">
                                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        <h1>Chitty Profile</h1>
                                        <fieldset>
                                            <h2>Chitty Profile</h2>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Name of Chitty *</label>
                                                        <input id="name" name="name" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Contact *</label>
                                                        <input id="contact" name="contact" type="text" class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Place </label>
                                                        <input id="place" name="place" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address *</label>
                                                        <input id="address" name="address" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>


                                        <h1>Show and Verify Chitty Details</h1>
                                        <fieldset>
                                            <h2>Chitty Details</h2>

                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group" id="data_1">
                                                        <label>Starting Date *</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="start_date" name="start_date" class="form-control" value="{{date('d-m-Y')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No.of Lots *</label>
                                                        <input id="no_of_lots" name="no_of_lots" type="number" min="1" max="500" value="50" class="form-control required">
                                                    </div>

                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Total Amount *</label>
                                                        <input id="total_kuri_amt" name="total_kuri_amt" type="number" min="1" value="100000" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Chitty Type *</label>
                                                        <select id="type" name="type" class="form-control">
                                                            <option value="1">Weekly</option>
                                                            <option value="2">Monthly</option>
                                                            <option value="3">Biweekly</option>
                                                            <option value="4">Daily</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>One Lot Amount *</label>
                                                        <input id="one_lot_amt" name="one_lot_amt" type="number" min="1" value="1000" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Have Organizor's Lot? *</label><br>
                                                        <select id="roganisor_lot" name="roganisor_lot" class="form-control">
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <h1>Finish</h1>
                                        <fieldset>
                                            <h2>Terms and Conditions</h2>
                                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                                            <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <!-- Data picker -->
    <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>


    <script>
        $(document).ready(function() {
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function(event, currentIndex, newIndex) {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex) {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18) {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function(event, currentIndex, priorIndex) {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18) {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3) {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function(event, currentIndex) {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function(event, currentIndex) {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                errorPlacement: function(error, element) {
                    element.before(error);
                },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $('#form').submit(function(event) {
                event.preventDefault();
                $.ajax({
                    url: "/submit-form",
                    type: "POST",
                    data: $('#form').serialize(),
                    success: function(response) {
                        alert(response.message); // handle the response
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error(textStatus, errorThrown); // handle any errors

                    }
                });
            });

            $(document).ready(function() {
                $('#acceptTerms').change(function() {
                    if (this.checked) {
                        $(this).prop('value', 1);
                    } else {
                        $(this).prop('value', 0);
                    }
                });
            });

        });
    </script>

</body>

</html>