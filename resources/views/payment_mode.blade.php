<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KURI App | Payment Modes</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')


            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Payment Types</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li class="active">
                                <strong>Payment Types</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 id="head_label"> Add New Payment Type</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    @if(session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                    @endif
                                    @if(session('failed'))
                                    <div class="alert alert-danger">
                                        {{ session('failed') }}
                                    </div>
                                    @endif
                                    <form id="form" method="post" action="{{route('paymentModeFormSubmit')}}" class="wizard-big">
                                        @csrf
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <input type="hidden" id="mode" name="mode">
                                                        <input type="hidden" id="kuri_id" name="kuri_id">
                                                        <label>Payment Type *</label>
                                                        <input id="name" name="name" type="text" class="form-control required" placeholder="Enter name of Payment Type">
                                                    </div>

                                                </div>
                                                <div class="col-lg-4">
                                                    <button id="btn-submit" type="submit" style="margin-top: 20px;" class="btn btn-success"><i class="fa fa-plus"></i> Create Payment Type</button>
                                                </div>
                                            </div>

                                        </fieldset>

                                    </form>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Payment Types</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 1;
                                                @endphp

                                                @foreach($payment_types as $type)
                                                <tr class="gradeX">
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $type->name }}</td>
                                                    <td>{{ $type->status }}</td>
                                                    <td><button data-id="{{ $type->id }}" title="Edit" class="btn btn-primary btn-edit"><i class="fa fa-edit"></i></button> &nbsp; <button data-id="{{ $type->id }}" title="Edit" class="btn btn-danger btn-edit"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                                @endforeach
                                            </tbody>


                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Payment Types</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>




            </div>

            <div class="modal" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="{{ asset('img/gpayQR.png') }}" alt="Your Image" style="width: 100%; height: auto;">
                        </div>
                    </div>
                </div>
            </div>

            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>

    <script>
        $(document).ready(function() {
            // $('.dataTables-example').DataTable({
            //     pageLength: 25,
            //     responsive: true,
            //     dom: '<"html5buttons"B>lTfgitp',
            //     buttons: [{
            //             extend: 'copy'
            //         },
            //         {
            //             extend: 'csv'
            //         },
            //         {
            //             extend: 'excel',
            //             title: 'ExampleFile'
            //         },
            //         {
            //             extend: 'pdf',
            //             title: 'ExampleFile'
            //         },

            //         {
            //             extend: 'print',
            //             customize: function(win) {
            //                 $(win.document.body).addClass('white-bg');
            //                 $(win.document.body).css('font-size', '10px');

            //                 $(win.document.body).find('table')
            //                     .addClass('compact')
            //                     .css('font-size', 'inherit');
            //             }
            //         }
            //     ]

            // });

            $('.btn-edit').on('click', function() {

                var id = $(this).data('id');
                $.ajax({
                    url: 'edit_pay_mode/' + id,
                    type: 'GET',
                    data: {
                        payId: id,
                    },
                    success: function(response) {
                        $('#name').val(response.data.name);
                        $('#head_label').text('Update Payment Type Details')
                        $('#btn-submit').text('Update Payment Type')
                        $('#mode').val('edit')
                        $('#kuri_id').val(response.data.id)
                    },
                    error: function(error) {
                        console.error('Ajax call error:', error);
                    }
                });
            });
            $('.btn-delete').on('click', function() {
                // Get the member ID and name
                var memId = $(this).data('id');
                var memberName = $(this).data('name');

                // Display a confirmation dialog
                var confirmed = confirm('Sure to Delete ' + memberName + '?');

                // Proceed with deletion if user confirms
                if (confirmed) {
                    $.ajax({
                        url: 'destroy/' + memId,
                        type: 'GET',
                        data: {
                            memId: memId,
                        },
                        success: function(response) {
                            alert('Deleted member succesfully')
                            location.reload();
                        },
                        error: function(error) {
                            console.error('Ajax call error:', error);
                        }
                    });
                }
            });
        });
    </script>

</body>

</html>