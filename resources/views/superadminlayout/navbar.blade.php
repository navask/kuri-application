<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <!-- <img alt="image" height="48px" width="48px" class="img-circle" src="img/profile_small.jpg" /> -->
                        <h1 style="color: white;">{{strtoupper(substr(Auth::user()->name, 0, 1))}}</h1>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong>
                            </span> <span class="text-muted text-xs block">Kuri Maintainer <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{url('user/profile')}}">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                {{strtoupper(substr(Auth::user()->name, 0, 1))}}
                </div>
            </li>

            @if( auth()->user()->user_type==1)
            <li class="active">
                <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="index.html">Dashboard v.1</a></li>
                    <li><a href="dashboard_2.html">Dashboard v.2</a></li>
                    <li><a href="dashboard_3.html">Dashboard v.3</a></li>
                    <li><a href="dashboard_4_1.html">Dashboard v.4</a></li>
                    <li class="active"><a href="dashboard_5.html">Dashboard v.5</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('make_new_chitti')}}"><i class="fa fa-plus"></i> <span class="nav-label">Generate Kuri</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Graphs</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="graph_flot.html">Flot Charts</a></li>
                    <li><a href="graph_morris.html">Morris.js Charts</a></li>
                    <li><a href="graph_rickshaw.html">Rickshaw Charts</a></li>
                    <li><a href="graph_chartjs.html">Chart.js</a></li>
                    <li><a href="graph_chartist.html">Chartist</a></li>
                    <li><a href="c3.html">c3 charts</a></li>
                    <li><a href="graph_peity.html">Peity Charts</a></li>
                    <li><a href="graph_sparkline.html">Sparkline Charts</a></li>
                </ul>
            </li>
            <li>
                <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Mailbox </span><span class="label label-warning pull-right">16/24</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="mailbox.html">Inbox</a></li>
                    <li><a href="mail_detail.html">Email view</a></li>
                    <li><a href="mail_compose.html">Compose email</a></li>
                    <li><a href="email_template.html">Email templates</a></li>
                </ul>
            </li>
            @else
             <li>
             <a href="#"><i class="fa fa-file"></i> <span class="nav-label">Kuri</span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{route('mark-payment-individual')}}"><i class="fa fa-user"></i> <span class="nav-label">Mark Payment</span></a></li>
                    <li><a href="{{route('view-kuri')}}"><i class="fa fa-users"></i> <span class="nav-label">View All Members</span></a></li>
                </ul>
            </li>
             <li>
                <a href="{{route('dashboard')}}"><i class="fa fa-user-plus"></i> <span class="nav-label">Add members</span></a>
            </li>
            <li>
                <a href="{{route('payment_modes')}}"><i class="fa fa-inr"></i> <span class="nav-label">Payment Types</span></a>
            </li>
             <li>
                <a href="{{route('reports')}}"><i class="fa fa-file"></i> <span class="nav-label">Reports</span></a>
            </li>
             <li>
                <a href="{{route('take_lot')}}"><i class="fa fa-tachometer "></i> <span class="nav-label">Take Lot</span></a>
            </li>
             <li>
                <a href="{{ route('logout') }}"><i class="fa fa-sign-out "></i> <span class="nav-label">Logout</span></a>
            </li>
          
            
            

            @endif
        </ul>

    </div>
</nav>