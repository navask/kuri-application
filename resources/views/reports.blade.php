<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KURI App | Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')


            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>View Reports</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('dashboard')}}">Home</a>
                            </li>
                            <li>
                                <a>My Kuri</a>
                            </li>
                            <li class="active">
                                <strong>Reports</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Generate Reports</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>



                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-2"><label for="">Choose Lot No. : </label>
                                                <select class="form-control" id="lot_no">
                                                    <option value="0">All</option>
                                                    @for($i=1;$i<=$no_of_lots;$i++) <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <button id="btn_search" class="btn btn-primary" style="margin-top: 22px;"> <i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="table-responsive">

                                    </div>

                                    <div class="modal" tabindex="-1" role="dialog" id="paymentModal">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Payment Collected?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form id="paymentForm" action="{{ route('mark.payment') }}" method="POST">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <label>
                                                            <input type="radio" name="paymentOption" value="M" checked> Manual
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="paymentOption" value="O"> Online
                                                        </label>

                                                        <input type="hidden" name="memId" id="memId">
                                                        <input type="hidden" name="lot_no" id="lot_no">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary" id="confirmPayment">OK</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {

            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function(win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

            $('#btn_search').on('click', function() {
                var lot_no = $('#lot_no').val()
                var html = ""
                $.ajax({
                    url: 'get_reports/' + lot_no,
                    type: 'GET',
                    data: {
                        lotNo: lot_no,
                    },
                    success: function(response) {
                        if (response.view_type == "all") {
                            html += "<table class='table table-striped table-bordered table-hover dataTables-example'><thead><tr><th>#</th><th>Member name & Contact</th>";
                            for (i = 1; i <= response.no_of_lots; i++) {
                                html += "<th>" + i + "</th>";
                            }
                            html += "</tr></thead><tbody>";
                            var sl = 1;
                            response.data.forEach(function(member) {
                                html += "<tr><td>" + sl++ + "</td><td>" + member.member_name + " - " + member.contact + "</td>";
                                for (j = 1; j <= response.no_of_lots; j++) {
                                    html += "<th>";

                                    if (member['lot_' + j] !== null) {
                                        html += "<strong style='color:green'>" + member.kuri_amount + "</strong>";
                                    } else {
                                        html += "<strong style='color:red'>No</strong>";
                                    }

                                    html += "</th>";
                                }
                                html += "</tr>";
                            });
                            html += "</tbody></table>";
                        } else {
                            html += "<table class='table table-striped table-bordered table-hover dataTables-example'><thead><tr><th>#</th><th>Member name & Contact</th><th>Amount</th><th>Payment mode</th></tr></thead><tbody>";
                            var sl = 1;
                            var totalAmount = 0;
                            var manualCount = 0;
                            var onlineCount = 0;

                            response.data.forEach(function(member) {
                                html += "<tr><td>" + sl++ + "</td><td>" + member.member_name + " - " + member.contact + "</td>";

                                if (member['lot_' + lot_no] === null) {
                                    html += "<td style='text-align:right'>0</td>";

                                } else {
                                    var kuriAmount = parseFloat(member.kuri_amount || 0); // Assuming kuri_amount is numeric
                                    html += "<td style='text-align:right'>" + kuriAmount + "</td>";
                                    totalAmount += kuriAmount;
                                }

                                    html += "<td>"+member['pay_mode_' + lot_no]+"</td>";
                                    manualCount++;

                                html += "</tr>";
                            });

                            html += "</tbody><tfoot><tr><th colspan='3' style='text-align:right'>Total : " + totalAmount.toFixed(2) + "</th><th colspan='1'>";
                            
                            response.payment_types.forEach(function(pay_type) {
                                html += pay_type.name+"<br>";
                            });
                            
                            html += "</th></tr></tfoot></table>";

                        }



                        $('.table-responsive').html(html)

                        console.log(response.data)
                        $('.dataTables-example').DataTable({
                            pageLength: 25,
                            responsive: true,
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [{
                                    extend: 'copy'
                                },
                                {
                                    extend: 'csv'
                                },
                                {
                                    extend: 'excel',
                                    title: 'ExampleFile'
                                },
                                {
                                    extend: 'pdf',
                                    title: 'ExampleFile'
                                },

                                {
                                    extend: 'print',
                                    customize: function(win) {
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                                    }
                                }
                            ]

                        });

                    },
                    error: function(error) {
                        console.error('Ajax call error:', error);
                        // Additional actions on error, if needed
                    }
                });
            });

        });
    </script>

</body>

</html>