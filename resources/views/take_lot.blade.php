<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KURI App | Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <style>
        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        .container {
            text-align: center;
        }

        .odometer-container {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .digit {
            font-size: 2em;
            width: 2em;
            margin: 0 5px;
            text-align: center;
            border: 1px solid #333;
            border-radius: 5px;
            background-color: #eee;
        }

        .buttons-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            margin-top: 20px;
        }

        button {
            margin: 5px;
        }

        .selected {
            background-color: yellow;
        }

        .congratulations {
            animation: bounce 0.8s ease-out;
        }

        @keyframes bounce {

            0%,
            20%,
            50%,
            80%,
            100% {
                transform: translateY(0);
            }

            40% {
                transform: translateY(-20px);
            }

            60% {
                transform: translateY(-10px);
            }
        }

        .selected-success {
            background-color: #28a745 !important;
            color: #fff;
        }

        #startButton {
            margin-top: 20px;
        }
    </style>

</head>

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')


            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Take Lot</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('dashboard')}}">Home</a>
                            </li>
                            <li>
                                <a>My Kuri</a>
                            </li>
                            <li class="active">
                                <strong>Take Lot</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Take Kuri Lot</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>



                                <div class="ibox-content">
                                    <div class="container">
                                        <div class="odometer-container" id="odometer">
                                            <div class="digit" id="digit1">0</div>
                                            <div class="digit" id="digit2">0</div>
                                            <div class="digit" id="digit3">0</div>
                                        </div>

                                        <div class="buttons-container" id="buttonsContainer"></div>

                                        <button class="btn btn-primary" id="startButton" onclick="startOdometer()">START</button>
                                    </div>
                                    <hr>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {



        });
    </script>
    <script>
        const valuesArray = [1, 2, 10, 15, 16, 17, 18, 20, 22, 26, 27, 28, 56, 102, 142];
        let isRunning = false;
        let selectedNumber = null;

        // Function to shuffle an array (Fisher-Yates algorithm)
        function shuffleArray(array) {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
        }

        // Shuffle the valuesArray
        shuffleArray(valuesArray);

        function startOdometer() {
            if (isRunning) return;

            const startButton = document.getElementById('startButton');
            startButton.disabled = true;

            const digit1 = document.getElementById('digit1');
            const digit2 = document.getElementById('digit2');
            const digit3 = document.getElementById('digit3');

            let currentNumber = 0;
            const startTime = Date.now();
            const endTime = startTime + 4000;

            const interval = setInterval(() => {
                const currentTime = Date.now();

                if (currentTime - startTime < 4000) {
                    currentNumber = valuesArray[Math.floor(Math.random() * valuesArray.length)];
                    const numString = currentNumber.toString().padStart(3, '0');
                    updateDigit(digit1, numString[0]);
                    updateDigit(digit2, numString[1]);
                    updateDigit(digit3, numString[2]);
                } else {
                    clearInterval(interval);
                    isRunning = false;
                    startButton.disabled = false;

                    // Highlight selected number in the list
                    highlightSelectedNumber();
                    // Add congratulations animation
                    setTimeout(() => {
                        digit1.classList.add('congratulations');
                        digit2.classList.add('congratulations');
                        digit3.classList.add('congratulations');
                    }, 1000);
                }
            }, 50);

            isRunning = true;
            selectedNumber = null; // Reset selected number
        }

        function updateDigit(digitElement, value) {
            digitElement.textContent = value;
            selectedNumber = value; // Update selected number during animation
        }

        // Display each number in the array in ascending order as a button
        const buttonsContainer = document.getElementById('buttonsContainer');
        valuesArray.sort((a, b) => a - b); // Sort numbers in ascending order
        valuesArray.forEach(number => {
            const button = document.createElement('button');
            button.className = 'btn btn-secondary';
            button.textContent = number;
            button.onclick = () => highlightSelectedNumber(button);
            buttonsContainer.appendChild(button);
        });

        function highlightSelectedNumber(clickedButton = null) {
            const buttons = document.getElementsByClassName('btn');
            Array.from(buttons).forEach(button => {
                button.classList.remove('selected', 'selected-success');
            });

            if (clickedButton) {
                clickedButton.classList.add('selected', 'selected-success');
            } else if (selectedNumber !== null) {
                const selectedButton = Array.from(buttons).find(button => parseInt(button.textContent) === selectedNumber);
                if (selectedButton) {
                    selectedButton.classList.add('selected', 'selected-success');
                }
            }
        }
    </script>

</body>

</html>