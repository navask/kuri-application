<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KURI App | Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')

            @if( auth()->user()->user_type==1)
            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Data Tables</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a>Tables</a>
                            </li>
                            <li class="active">
                                <strong>Data Tables</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Wizard with Validation</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <h2>
                                        Validation Wizard Form
                                    </h2>
                                    <p>
                                        This example show how to use Steps with jQuery Validation plugin.
                                    </p>

                                    <form id="form" action="#" class="wizard-big">
                                        <h1>Account</h1>
                                        <fieldset>
                                            <h2>Account Information</h2>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group">
                                                        <label>Username *</label>
                                                        <input id="userName" name="userName" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password *</label>
                                                        <input id="password" name="password" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Confirm Password *</label>
                                                        <input id="confirm" name="confirm" type="text" class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="text-center">
                                                        <div style="margin-top: 20px">
                                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </fieldset>
                                        <h1>Profile</h1>
                                        <fieldset>
                                            <h2>Profile Information</h2>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>First name *</label>
                                                        <input id="name" name="name" type="text" class="form-control required">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last name *</label>
                                                        <input id="surname" name="surname" type="text" class="form-control required">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Email *</label>
                                                        <input id="email" name="email" type="text" class="form-control required email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address *</label>
                                                        <input id="address" name="address" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <h1>Warning</h1>
                                        <fieldset>
                                            <div class="text-center" style="margin-top: 120px">
                                                <h2>You did it Man :-)</h2>
                                            </div>
                                        </fieldset>

                                        <h1>Finish</h1>
                                        <fieldset>
                                            <h2>Terms and Conditions</h2>
                                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                                            <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            @else
            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Dashboard</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a>Dashboard</a>
                            </li>
                            <li class="active">
                                <strong>My Dashboard</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5 id="head_label"> Add New Member</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    @if(session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                    @endif
                                    @if(session('failed'))
                                    <div class="alert alert-danger">
                                        {{ session('failed') }}
                                    </div>
                                    @endif
                                    <form id="form" method="post" action="{{route('memberFormSubmit')}}" class="wizard-big">
                                        @csrf
                                        <fieldset>
                                            <input type="hidden" name="mode" id="mode">
                                            <input type="hidden" name="member_id" id="member_id">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Member Name *</label>
                                                        <input id="name" name="name" type="text" class="form-control required" placeholder="Enter member name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Lot Number *</label>
                                                        <input id="lot_number" name="lot_number" type="number" class="form-control required" placeholder="Enter Lot number">
                                                    </div>


                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Contact Number *</label>
                                                        <input id="contact" name="contact" type="text" class="form-control required" placeholder="Enter contact number">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Lot Amount *</label>
                                                        <input id="kuri_amount" name="kuri_amount" type="number" class="form-control required" placeholder="Enter member's One Lot Amount">
                                                    </div>
                                                </div>

                                            </div>

                                        </fieldset>
                                        <center><button id="btn-submit" type="submit" class="btn btn-success"><i class="fa fa-user-plus"></i> Create Member</button></center>
                                    </form>
                                    <br>

                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5> All Members</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example">

                                            <thead>
                                                <tr>
                                                    <th>Sl.No.</th>
                                                    <th>Lot No.</th>
                                                    <th>Name & Mobile Number</th>
                                                    <th>Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $i=1; @endphp
                                                @foreach($members_list as $member)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$member->lot_number}}</td>
                                                    <td>{{$member->member_name}} - {{$member->contact}}</td>
                                                    <td>{{$member->kuri_amount}} </td>
                                                    <td><button title="Edit" data-id="{{$member->id}}" class="btn-edit btn btn-primary"><i class="fa fa-pencil"></i></button> &nbsp; <button data-id="{{$member->id}}" data-name="{{$member->member_name}}" title="Delete" class="btn-delete btn btn-danger"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sl.No.</th>
                                                    <th>Lot No.</th>
                                                    <th>Name & Mobile Number</th>
                                                    <th>Amount</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>




            </div>

            <div class="modal" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="{{ asset('img/gpayQR.png') }}" alt="Your Image" style="width: 100%; height: auto;">
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="js/plugins/dataTables/datatables.min.js"></script>


    <script>
        $(document).ready(function() {
            // $('#imageModal').modal('show');


            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function(win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });


            $(document).on('click', '.btn-edit', function() {
                var memId = $(this).data('id')
                $.ajax({
                    url: 'edit/' + memId,
                    type: 'GET',
                    data: {
                        memId: memId,
                    },
                    success: function(response) {
                        $('#name').val(response.data.member_name)
                        $('#contact').val(response.data.contact)
                        $('#lot_number').val(response.data.lot_number)
                        $('#kuri_amount').val(response.data.kuri_amount)
                        $('#member_id').val(response.data.id)
                        $('#mode').val('edit')

                        $('#head_label').text('Update Member Details')
                        $('#btn-submit').text('Update Member')
                    },
                    error: function(error) {
                        console.error('Ajax call error:', error);
                    }
                });
            });

            $(document).on('click', '.btn-delete', function() {
                // Get the member ID and name
                var memId = $(this).data('id');
                var memberName = $(this).data('name');

                // Display a confirmation dialog
                var confirmed = confirm('Sure to Delete ' + memberName + '?');

                // Proceed with deletion if user confirms
                if (confirmed) {
                    $.ajax({
                        url: 'destroy/' + memId,
                        type: 'GET',
                        data: {
                            memId: memId,
                        },
                        success: function(response) {
                            alert('Deleted member succesfully')
                            location.reload();
                        },
                        error: function(error) {
                            console.error('Ajax call error:', error);
                        }
                    });
                }
            });

        });
    </script>

</body>

</html>