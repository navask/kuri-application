<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>KURI App | Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
    <div id="wrapper">
        @include('superadminlayout.navbar')

        <div id="page-wrapper" class="gray-bg">
            @include('superadminlayout.headerbar')


            <div class="wrapper wrapper-content animated fadeIn">
                <!-- contents -->

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>View Kuri Details</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('dashboard')}}">Home</a>
                            </li>
                            <li>
                                <a>My Kuri</a>
                            </li>
                            <li class="active">
                                <strong>View</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>View All members and mark the cash payments</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">

                                    <center>
                                        <div id="datetime" style="font-size: large;font-weight: bold;"></div>
                                    </center>
                                    <hr>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th colspan="4"></th>
                                                    @php
                                                    $j = 0;
                                                    $today = now()->format('d-m-Y');
                                                    $lastSunday = Carbon\Carbon::parse('last sunday')->format('d-m-Y');
                                                    @endphp

                                                    @for ($i = $firstDatePosition; $i < $firstDatePosition + count($sundays); $i++) @php $isToday=($sundays[$j]===$today); $isLastSunday=($sundays[$j]===$lastSunday); $shouldApplyBorder=$isToday && !$isLastSunday; @endphp <th title="{{ $sundays[$j] }}" style="{{ $shouldApplyBorder ? 'border: 1px solid green;' : '' }}">
                                                        <span data-date="{{ $sundays[$j] }}">{{ $sundays[$j] }}</span>
                                                        </th>
                                                        @php $j++; @endphp
                                                        @endfor
                                                </tr>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Member Name</th>
                                                    <th>Lot No</th>
                                                    <th>Amount</th>
                                                    @php
                                                    $j = 0;
                                                    $today = now()->format('d-m-Y');
                                                    $lastSunday = Carbon\Carbon::parse('last sunday')->format('d-m-Y');
                                                    @endphp

                                                    @for ($i = $firstDatePosition; $i < $firstDatePosition + count($sundays); $i++) @php $isToday=($sundays[$j]===$today); @endphp <th title="{{ $sundays[$j] }}" style="{{ $isToday ? 'border: 1px solid green;' : '' }}">
                                                        <span data-date="{{ $sundays[$j] }}">{{ $i }}</span>
                                                        </th>
                                                        @php $j++; @endphp
                                                        @endfor
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i = 1;
                                                $today = now()->format('d-m-Y');
                                                @endphp

                                                @foreach($members_list as $member)
                                                <tr class="gradeX">
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $member->member_name }}</td>
                                                    <td>{{ $member->lot_number }}</td>
                                                    <td>{{ $member->kuri_amount }}</td>
                                                    @php
                                                    $l = 0;
                                                    @endphp

                                                    @for ($j = $firstDatePosition; $j < $firstDatePosition + count($sundays); $j++) @php $isToday=($sundays[$l]===$today); $applyGreenBorder=$isToday; @endphp <td style="{{ $applyGreenBorder ? 'border: 1px solid green;' : '' }}">
                                                        @if ($member->{'lot_' . $j} === null)
                                                        <button title="Lot : {{ $j }}  Date: {{$sundays[$l]}}" type="button" data-memid="{{ $member->id }}" data-lot_no="{{ 'lot_' . $j }}" class="btn_mark" data-toggle="modal" data-target="#paymentModal">
                                                            <i class="fa fa-times" style="{{ $isToday ? 'color: red;' : 'color: red;' }}"></i>
                                                        </button>
                                                        @else
                                                        <button data-toggle="popover" data-placement="top" title="Lot No:{{$j}} Date: {{$sundays[$l]}}  Pay Type: {{ $member->{'pay_mode_' . $j} }}" data-content="{{ $member->{'note_' . $j} }}">
                                                            <i class="fa fa-check" style="{{ $applyGreenBorder ? 'color: green;' : 'color: green;' }}"></i>
                                                        </button>
                                                        @endif
                                                        </td>
                                                        @php $l++; @endphp
                                                        @endfor
                                                </tr>
                                                @endforeach
                                            </tbody>




                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Member Name</th>
                                                    <th>Lot No</th>
                                                    <th>Amount</th>
                                                    @php
                                                    $j = 0;
                                                    $today = now()->format('d-m-Y');
                                                    $lastSunday = Carbon\Carbon::parse('last sunday')->format('d-m-Y');
                                                    @endphp

                                                    @for ($i = $firstDatePosition; $i < $firstDatePosition + count($sundays); $i++) @php $isToday=($sundays[$j]===$today); @endphp <th title="{{ $sundays[$j] }}" style="{{ $isToday ? 'border: 1px solid green;' : '' }}">
                                                        <span data-date="{{ $sundays[$j] }}">{{ $i }}</span>
                                                        </th>
                                                        @php $j++; @endphp
                                                        @endfor
                                                </tr>


                                                <tr>
                                                    <th colspan="4"></th>
                                                    @php
                                                    $j = 0;
                                                    $today = now()->format('d-m-Y');
                                                    $lastSunday = Carbon\Carbon::parse('last sunday')->format('d-m-Y');
                                                    @endphp

                                                    @for ($i = $firstDatePosition; $i < $firstDatePosition + count($sundays); $i++) @php $isToday=($sundays[$j]===$today); $isLastSunday=($sundays[$j]===$lastSunday); $shouldApplyBorder=$isToday && !$isLastSunday; @endphp <th title="{{ $sundays[$j] }}" style="{{ $shouldApplyBorder ? 'border: 1px solid green;' : '' }}">
                                                        <span data-date="{{ $sundays[$j] }}">{{ $sundays[$j] }}</span>
                                                        </th>
                                                        @php $j++; @endphp
                                                        @endfor
                                                </tr>

                                            </tfoot>
                                        </table>
                                    </div>

                                    <div class="modal" tabindex="-1" role="dialog" id="paymentModal">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Payment Collected?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form id="paymentForm" action="{{ route('mark.payment') }}" method="POST">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <!-- <label>
                                                            <input type="radio" name="paymentOption" value="M" checked> Manual
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="paymentOption" value="O"> Online
                                                        </label> -->
                                                        @foreach($payment_types as $key => $type)
                                                        <label>
                                                            <input type="radio" name="paymentOption" value="{{$type->name}}" @if($key===0) checked @endif> {{$type->name}}
                                                        </label><br>
                                                        @endforeach


                                                        <textarea class="form-control" name="note" id="note" placeholder="Write short note"></textarea>

                                                        <input type="hidden" name="memId" id="memId">
                                                        <input type="hidden" name="lot_no" id="lot_no">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary" id="confirmPayment">OK</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @include('superadminlayout.footer')
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>


    <!-- Steps -->
    <script src="js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>

    <script>
        function updateDateTime() {
            var now = new Date();
            var dateStr = now.toDateString();
            var timeStr = now.toLocaleTimeString();

            $('#datetime').html(dateStr + ' ' + timeStr);
        }

        // Update every second
        setInterval(updateDateTime, 1000);

        // Initial update
        updateDateTime();
    </script>
    <script>
        $(document).ready(function() {

            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [{
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'ExampleFile'
                    },
                    {
                        extend: 'pdf',
                        title: 'ExampleFile'
                    },

                    {
                        extend: 'print',
                        customize: function(win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

            $('.btn_mark').on('click', function() {
                var memId = $(this).data('memid');
                var lotNo = $(this).data('lot_no');

                $('#memId').val(memId)
                $('#lot_no').val(lotNo)
            });

            $(function() {
                $('[data-toggle="popover"]').popover();
            });

        });
    </script>

</body>

</html>