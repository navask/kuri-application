<?php

use App\Http\Controllers\KuriController;
use App\Models\Kuri;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('dashboard', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $members_list = DB::table($table)->select('id', 'member_name', 'contact', 'kuri_amount', 'lot_number')->orderBy('lot_number')->get();
        return view('dashboard', compact('members_list'));
    })->name('dashboard');

    Route::get('view-kuri', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $start_date = Kuri::where('table_name', $table)->first(['start_date']);
        $no_of_lots = $no_of_lots->no_of_lots;
        $start_date = $start_date->start_date;

        $today = Carbon::now();
        $todayIsSunday = ($today->dayOfWeek == Carbon::SUNDAY);

        // If today is not Sunday, find the last passed Sunday
        if (!$todayIsSunday) {
            $today = $today->previous(Carbon::SUNDAY);
        }

        $sundays = [];
        for ($i = -5; $i <= 4; $i++) {
            $sundays[] = $today->copy()->addDays(7 * $i)->format('d-m-Y');
        }


        $start_date = Carbon::createFromFormat('d-m-Y', $start_date);

        // Find the position of the first date in $sundays relative to $start_date
        $firstDatePosition = $start_date->diffInDays(Carbon::createFromFormat('d-m-Y', $sundays[0])) / 7;
// var_dump($sundays[0]);exit;
        $members_list = DB::table($table)->orderBy('lot_number')->get();
        $payment_types = DB::table('payment_modes')->where(['status' => 'active', 'created_by' => auth()->user()->email])->get();
        $firstDatePosition = $firstDatePosition + 1;
        return view('kuri_view', compact('members_list', 'no_of_lots', 'payment_types', 'start_date', 'sundays', 'firstDatePosition'));
    })->name('view-kuri');

    Route::get('mark-payment-individual', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $start_date = Kuri::where('table_name', $table)->first(['start_date']);
        $no_of_lots = $no_of_lots->no_of_lots;
        $start_date = $start_date->start_date;

        $today = Carbon::now();
        $todayIsSunday = ($today->dayOfWeek == Carbon::SUNDAY);

        // If today is not Sunday, find the last passed Sunday
        if (!$todayIsSunday) {
            $today = $today->previous(Carbon::SUNDAY);
        }

        $sundays = [];
        for ($i = -5; $i <= 4; $i++) {
            $sundays[] = $today->copy()->addDays(7 * $i)->format('d-m-Y');
        }


        $start_date = Carbon::createFromFormat('d-m-Y', $start_date);

        // Find the position of the first date in $sundays relative to $start_date
        $firstDatePosition = $start_date->diffInDays(Carbon::createFromFormat('d-m-Y', $sundays[0])) / 7;

        $members_list = DB::table($table)->orderBy('lot_number')->get();
        $payment_types = DB::table('payment_modes')->where(['status' => 'active', 'created_by' => auth()->user()->email])->get();
        $firstDatePosition = $firstDatePosition + 1;
        $amounts = DB::table($table)->select('kuri_amount')->distinct()->orderBy('kuri_amount')->get();
        $currentDate = date('d-m-Y');
        $position = array_search($currentDate, $sundays);
        $curr_date_pos= $firstDatePosition+5;
        return view('mark_payment', compact('members_list', 'no_of_lots', 'payment_types', 'start_date', 'sundays', 'firstDatePosition','amounts','curr_date_pos'));
    })->name('mark-payment-individual');

    Route::get('reports', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $no_of_lots = $no_of_lots->no_of_lots;

        $members_list = DB::table($table)->orderBy('lot_number')->get();
        return view('reports', compact('members_list', 'no_of_lots'));
    })->name('reports');

    Route::get('take_lot', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $no_of_lots = $no_of_lots->no_of_lots;

        $members_list = DB::table($table)->orderBy('lot_number')->get();
        return view('take_lot', compact('members_list', 'no_of_lots'));
    })->name('take_lot');

    Route::get('MakeNewChitti', function () {
        return view('makekuri.make_kuri');
    })->name('make_new_chitti');
    Route::get('PaymentModes', function () {
        $payment_types = DB::table('payment_modes')->where(['created_by' => auth()->user()->email])->get();
        return view('payment_mode', compact('payment_types'));
    })->name('payment_modes');

    // Route::post('CreateNewChitti', function () {
    //     return view('makekuri.make_kuri');
    // })->name('create_new_chitti');
    Route::post('submit-form', [KuriController::class, 'store'])->name('myFormSubmit');
    Route::post('create-member', [KuriController::class, 'addMember'])->name('memberFormSubmit');
    Route::post('paymentModeFormSubmit', [KuriController::class, 'createPayMode'])->name('paymentModeFormSubmit');
    Route::post('mark-payment', [KuriController::class, 'mark_payment'])->name('mark.payment');
    Route::post('mark_payment_individual', [KuriController::class, 'mark_payment_individual'])->name('mark.payment_individual');
    Route::get('/get_reports/{lot_no}', [KuriController::class, 'get_reports'])->name('get.reports');
    Route::get('/edit/{id}', [KuriController::class, 'edit'])->name('edit');
    Route::get('/destroy/{id}', [KuriController::class, 'destroy'])->name('delete');
    Route::get('/edit_pay_mode/{id}', [KuriController::class, 'edit_pay_mode'])->name('edit_pay_mode');
    Route::get('/get_date_and_amount/{mem_id}/{lot}', [KuriController::class, 'get_date_and_amount'])->name('get_date_and_amount');
});

Route::get('logout', function () {
    Auth::logout();
    return redirect('login');
})->middleware('auth')->name('logout');
