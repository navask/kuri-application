<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentModesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_modes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('kuri_id');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->string('created_by');

            // Define foreign key constraint
            $table->foreign('kuri_id')->references('id')->on('kuris');

            // Add any additional indexes or constraints you need
            $table->index('name');
            $table->unique('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_modes');
    }
}
