<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKurisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuris', function (Blueprint $table) {
            $table->id();
            $table->string('userName')->unique();
            $table->string('password');
            $table->string('confirm');
            $table->string('name');
            $table->string('contact');
            $table->string('place');
            $table->string('address');
            $table->integer('no_of_lots');
            $table->string('type');
            $table->string('roganisor_lot');
            $table->boolean('acceptTerms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuris');
    }
}
